# DAO - DATA ACCCESS OBJECT  #

The pattern, Data Access Object (DAO), is a class that provides a mechanism  an abstract interface to some type of database or other persistence mechanism. 
The idea of DAO, is mapping application calls to the persistence layer, the DAO provides some specific data operations without exposing details of the database, protecting the database.
Instead of having your classes, application, code, communicating directly with the database, file system, webservice's, or other persistence mechanism, your application uses the domain logic to speak with DAO layer instead. With this layer of abstraction, will separate what data access the aplication needs, in terms of domain specific objects and data types (your class of abstraction of the DAO), from how these needs can be satisfied with a specific database, file system, or other mechanism or saving data.

We will explain DAO with PHP, but this design pattern is equally applicable to most programming languages, most types of databases.

The  advantage of using this Pattern, is all advantages is greater than disadvantages.

### Blocks to understand DAO
```
|---------|   |-------|   |-----------|
|  CODE   |-->|  DAO  |-->|  DATABASE |
|---------|   |-------|   |-----------|
```
```
|----------|   |----------|   |-------|   |-----------|
|  VIEWS   |-->|  CLASSES |-->|  DAO  |-->|  DATABASE |  
|----------|   |----------|   |-------|   |-----------|
```

### Advantages ###

* Separation by Layers your Application (At least two parts)
* Relatively simple
* Changes occurs only at DAO interface, when changing bussiness logic
* Changes at persistence Logic not affect DAO
* Can support multiple database types
* Standardization of projects and rules of persistence
* Ease of use of other means of persistence

### Disadvantages ###

* If your project uses more than one databases, it's more complicated
* Designing DAO can be problem when connection scope
* For that, you can create a generic scope for all methods, where you have a single internal connection that all methods use
* Or each DAO method that open and close it's own connection
* Or can use other Pattern, that verify if conncetion is already open or not, for example with Singleton Pattern
* With this problem, have many solutions to apply

### What is this repository for? ###

* Understand DAO Pattern
* Examples to implement DAO


### Example 1:  Simple to understand the DAO ###

This example is simple to understand DAO, was developed with PHP, and use CRUD (Create Read Update Delete), and it's show only the class/Layer of DAO, which is extended by class Example. 

After you understand DAO, you can implement in many ways, like example 2. 


### Example 2:  Another and better way to implemet DAO ###

In this example, class DAO is more abstract with persisent layer, and this example was developed to use MySQL and PostGreSQL, which uses CRUD, Singleton, where your Layer of DAO, will instance automatically.

This is another example of DAO, and developers can create with more abstraction DAO Layer, where DAO class can have method like 'fetch', where fetch data from file system, database like MySQL, NoSQL, other others. For example, instead of using commands such as insert, delete, and update to access a specific table in a database, a class and a few stored procedures could be created in the database. The procedures would be called from a method inside the class, which would return an object containing the requested values. Or, the insert, delete and update commands could be executed within simple functions like fetch, login, stored methods, within the data access layer

The frist thing to do when creating the DAO, is making your specifications and needs, to understand which sis the best way to create DAO.


### Authors ###

* Ricardo Sousa 

