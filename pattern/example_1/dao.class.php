<?php

/**
 * Class DAO
 */
abstract class DAO
{
    /**
     * @var
     */
    static $_connection;
    static $_db_host;
    static $_db_port;
    static $_db_name;
    static $_db_user;
    static $_db_passwd;

    /**
     * @var
     */
    protected $_query;
    protected $_table;

    /**
     * DAO constructor.
     */
    public function __construct($_table)
    {
        self::$_db_host     = "";
        self::$_db_port     = "";
        self::$_db_name     = "";
        self::$_db_user     = "";
        self::$_db_passwd   = "";

        $this->_table       = $_table;
        $this->_connect_to_db();
    }

    /**
     *
     */
    private function _connect_to_db()
    {
        self::$_connection = new PDO("pgsql: host='".self::$_db_host."'; port='".self::$_db_port."'; dbname='".self::$_db_name."'; user='".self::$_db_user."'; password='".self::$_db_passwd."'");
        self::$_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * @param $_key
     * @param $_value
     * @return string
     */
    public function create($_key, $_value)
    {
        $this->_query = "INSERT INTO $this->_table ($_key) VALUES ($_value)";

        return $this->execute_query();
    }

    /**
     * @param $_key
     * @param $_value
     * @return string
     */
    public function read($_key, $_value)
    {
        $this->_query = "SELECT * FROM $this->_table WHERE $_key=$_value";

        return $this->execute_query();
	}

    /**
     * @param $_key_to_set
     * @param $_value_to_set
     * @param $_key
     * @param $_value
     * @return string
     */
    public function update($_key_to_set, $_value_to_set, $_key, $_value)
    {
        $this->_query = "UPDATE $this->_table SET $_key_to_set=$_value_to_set WHERE $_key=$_value";

        return $this->execute_query();
    }

    /**
     * @param $_key
     * @param $_value
     * @return string
     */
    public function delete($_key, $_value)
    {
        $this->_query = "DELETE FROM $this->_table WHERE $_key=$_value";

        return $this->execute_query();
    }

    /**
     * @return string
     */
    private function execute_query()
    {
        $_stmt = self::$_connection->prepare($this->_query);

        try
        {
            $_stmt = self::$_connection->prepare($this->_query);
            if ($_stmt->execute())
            {
                return $_stmt->fetchAll(PDO::FETCH_ASSOC);
            }

        } catch (Exception $e) {

            return "error_query";

        }
    }

    /**
     * @param $_table
     */
    public function which_table($_table)
    {
        $this->_table = $_table;
    }
}



class Example extends DAO
{
    /**
     * @param $_id
     * @return mixed
     */
    public function get_my_information_user($_id)
    {
        // Getting information through DAO
        $result = $this->fetch('id', $_id);

        return $result;
    }
}

// Lets get the instance of class Example
$_instance      = new Example("users");
// Now lets get directly the information to DAO, because of extends
$_my_info_user  = $_instance->read('id',1);
// The only difference it's you can choose table for every method you call
$_my_info_user  = $_instance->which_table("users")->read('id',1);
// Getting information of user with method of Example
$_info_user     = $_instance->get_my_information_user(1);