<?php

class Database
{

    /**
     * @var Private
     */
    private $_query               = null;
    private $_table               = null;
    private $_extra_query         = null;
    private $_extra_query_and_or  = null;
    private $_from_query          = null;
    private $_inner_join          = null;
    private $_type_query          = null;
    private $_return_id           = null;
    private $_set_fields          = null;

    /**
     * @var Self
     */
    static $_connection;
    static $_db_host;
    static $_db_port;
    static $_db_name;
    static $_db_user;
    static $_db_passwd;

    /**
     * Database constructor.
     * @param $_schema
     */
    public function __construct()
    {
        self::$_db_host     = "";
        self::$_db_port     = "";
        self::$_db_name     = "";
        self::$_db_user     = "";
        self::$_db_passwd   = "";

        $this->_connect_to_db();
    }

    /**
     * @return PDO|Self
     */
    static function _connect_to_db()
    {
        // Verify if already have connector to DB
        if (self::$_connection == null)
        {
            self::$_connection = new PDO("pgsql: host='".self::$_db_host."'; port='".self::$_db_port."'; dbname='".self::$_db_name."'; user='".self::$_db_user."'; password='".self::$_db_passwd."'");
            self::$_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }

        return self::$_connection;
    }

    /** CREATE
     * @param $_fields_to_insert
     * @param null $_optional_insert
     * @return $this
     */
    public function insert($_fields_to_insert, $_optional_insert = null)
    {
        $this->_type_query = "insert";

        // Construct query
        if($_optional_insert)
        {
            $this->_query = " INSERT INTO  ".$this->_schema.".".$this->_table." ".$_optional_insert;
        }
        else
        {
            $this->_query = " INSERT INTO ".$this->_schema.".".$this->_table." ($_fields_to_insert) ";
        }

        return $this;
    }

    /*
    * Principal Methods DAO for CRUD Databases like MySQL, PostGreSQL
    */
    /** READ
     * @param $_fields_to_select
     * @return $this
     */
    public function select($_fields_to_select)
    {
        $this->_type_query = "select";

        // Construct query
        $this->_query = "SELECT $_fields_to_select ";

        return $this;
    }

    /** UPDATE
     * @param $_set_fields
     * @return $this
     */
    public function update($_set_fields)
    {
        $this->_type_query = "update";

        // Construct query
        $this->_query = "UPDATE ";

        $this->_set_fields = " SET $_set_fields ";
        // do something
        return $this;
    }

    /** DELETE
     * @return $this
     */
    public function delete()
    {
        $this->_type_query = "delete";

        // Construct query
        $this->_query = "DELETE ";

        // do something
        return $this;
    }

    /*
     * Adicional Methods DAO for CRUD Databases like MySQL, PostGreSQL
     */
    /**
     * @param $_values_to_insert
     * @param null $_next_value
     * @return $this
     */
    public function values($_values_to_insert, $_next_value = null)
    {
        // Construct query
        if($_next_value)
        {
            $this->_query.= " , ($_values_to_insert) ";
        }
        else
        {
            $this->_query.= " VALUES ($_values_to_insert) ";
        }

        return $this;
    }

    /**
     * @param $_from_forced
     * @return $this
     */
    public function from($_table)
    {
        $this->_from_query = " FROM $_table ";

        return $this;
    }

    /**
     * @param $_table
     * @return $this
     */
    public function table($_table)
    {
        // define table
        $this->_table = $_table;

        return $this;
    }

    /**
     * @param $_table_with_short_name
     * @param $_on
     * @return $this
     */
    public function inner_join($_table_with_short_name, $_on)
    {
        $this->_extra_query.= " INNER JOIN $_table_with_short_name ON $_on ";

        return $this;
    }

    /**
     * @param $_table_with_short_name
     * @param $_on
     * @return $this
     */
    public function _left_join($_table_with_short_name, $_on)
    {
        $this->_extra_query.= " LEFT JOIN $_table_with_short_name ON $_on ";

        return $this;
    }

    /**
     * @param $_table_with_short_name
     * @param $_on
     * @return $this
     */
    public function _right_join($_table_with_short_name, $_on)
    {
        $this->_extra_query.= " LEFT JOIN $_table_with_short_name ON $_on ";

        return $this;
    }

    /**
     * @param $_conditions
     * @return $this
     */
    public function where($_conditions)
    {
        $this->_extra_query.= " WHERE $_conditions ";
        return $this;
    }

    /**
     * @param $_conditions
     * @return $this
     */
    public function _and($_conditions)
    {
        $this->_extra_query.= " AND $_conditions ";
        return $this;
    }

    /**
     * @param $_conditions
     * @return $this
     */
    public function _or($_conditions)
    {
        $this->_extra_query.= " OR $_conditions ";
        return $this;
    }

    /**
     * @param $_group_by
     * @return $this
     */
    public function group_by($_group_by)
    {
        $this->_extra_query.= " GROUP BY $_group_by ";
        return $this;
    }

    /**
     * @param $_order_by
     * @return $this
     */
    public function order_by($_order_by)
    {
        $this->_extra_query.= " ORDER BY $_order_by ";
        return $this;
    }

    /**
     * @param $_limit
     * @return $this
     */
    public function limit($_limit)
    {
        $this->_extra_query.= " LIMIT $_limit ";
        return $this;
    }

    /**
     * @param $_field
     * @return $this
     */
    public function returning($_field)
    {
        $this->_return_id = " RETURNING ".$_field;
        RETURN $this;
    }

    public function end()
    {
        switch ($this->_type_query)
        {
            case "select":
                    $this->_query.= $this->_from_query.$this->_extra_query;
                break;
            case "insert":
                $this->_query.= $this->_table.$this->_set_fields." ".$this->_extra_query;
                break;
            case "update":
                $this->_query.= $this->_table.$this->_set_fields." ".$this->_extra_query;
                break;
            case "delete":
                $this->_query.= $this->_from_query.$this->_extra_query;
                break;
            case "free_exclusive":
                $this->_query    = $this->_free_execute;
                $_free_exclusive = true;
                break;
        }

        // For Returning ID it's the last addiction
        if(!empty($this->_return_id))
        {
            $this->_query.= $this->_return_id;
        }

        try
        {
            // Preparing query, PDO verify if all it's ok
            $_stmt = self::$_connection->prepare($this->_query);

            if ($_stmt->execute())
            {
                // Lets Fetch All
                $_data = $_stmt->fetchAll(PDO::FETCH_ASSOC);
                // Return Data or error 404 - Not Found
                return (!empty($_data) ? $_data : 404);
            }
            else
            {
                // Lets return 406 - Not Acceptable
                return 406;
            }

        } catch (Exception $e) {

            // Lets return 406 - Not Acceptable
            return 406;

        }
        // Lets return 406 - Not Acceptable
        return 406;
    }
}

/**
 * Class DB
 */
class DB
{
    /**
     * @var
     */
    private static $instance = NULL;

    /**
     * @return mixed
     */
    public static function database()
    {
        // SingleTon Pattern
        // This is for Databases like MySQL or PostGreSQL
        if (self::$instance === NULL)
        {
            self::$instance = new Database();
        }
        return self::$instance_sensor;
    }
}

/**
 * Class example
 */
Class example
{
    public function get_all_info_user()
    {
        // Dao Layer
        return DB::database()->select("*")->from("users")->where("name='XPTO'")->and("id=5")->end();
    }
}
// Example Instance
$_instance_example = new Example ();
// Callmethod, that will call DAO Layer
$_instance_example->get_all_info_user();